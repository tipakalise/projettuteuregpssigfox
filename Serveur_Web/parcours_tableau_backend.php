<?php
include('convert.php');
foreach ($data['data'] as $reg) {
    $donnee = $reg['data'];
    
    ?>
    <tr>

        <?php
        //Si le champs data n'est pas vide, n'affiche pas le tableau
        if ($reg['data'] != NULL) {
            echo "<td>" . date(DATE_RFC2822, $reg['time']) . "</td>";
            echo "<td>" . $donnee . "</td>";
            echo "<td> " . $reg['snr'] . "</td>";
            echo "<td>";
            $qualite = $reg['linkQuality'];
            switch ($qualite) {
                case "EXCELLENT":
                    echo "Excellente";
                    break;
                case "GOOD":
                    echo "Bonne";
                    break;
                case "AVERAGE":
                    echo "Moyenne";
                    break;
                case "LIMIT" :
                    echo "Faible";
                    break;
            }

            echo "</td>";
        }
        ?>  

    </tr>

    <?php
}
?>

