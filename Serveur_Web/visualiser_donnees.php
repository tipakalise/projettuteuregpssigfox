<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/affichage_map.css" />
        <title>Consultation Data SIGFOX</title>
    </head>
    <body>
        <span class="metadata-marker" style="display: none;" data-region_tag="html-body"></span>
        <h3>Visualisation des données GPS</h3>

        <!--div faisant référence à la carte -->
        <div id="map"></div>
        
        <?php
        
        $long = $_GET['long'];
        $lat = $_GET['lat'];
        
        echo "<br>";
        echo "<b>Latitude =</b> " . $lat;
        echo "<br>";
        echo "<b>Longitude =</b> " . $long; 
        echo "<br><br>";
        echo "<a href='Recuperation-donnees-API.php'><input type='button' value='Retour'></a>"
        
        ?>

        <script>

            // Initialisation et ajout de la carte
            function initMap() {

                // Notre position
                var position = {lat: <?php echo $lat ?>, lng: <?php echo $long ?>};

                // Centrer la carte autour de la position
                var map = new google.maps.Map(
                        document.getElementById('map'), {zoom: 18, center: position});

                // POur le pointeur rouge situé au dessus de notre position
                var pointeur = new google.maps.Marker({position: position, map: map});
            }
        </script>

        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3D1lLB3ZMMD-s2MNwPFV7ednvZ_wZmPw&callback=initMap">
        </script>
    </body>
</html>