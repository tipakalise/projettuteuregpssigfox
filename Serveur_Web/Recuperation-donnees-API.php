﻿<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/style.css" />
        <title>Consultation Data SIGFOX</title>
        <?php
        include('Connexion_sigfox.php');
        ?>
    </head>
    <body>
        <br />
        <img src="img/logo.png">
        <br />
        <h2>Récupération données SigFox par l'API</h2>
        <table>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Données</th>
                    <th>SNR</th>
                    <th>Qualité de la Liaison</th>
                </tr>
            </thead>
            <tbody>
                <?php
                include('parcours_tableau_backend.php');
                ?>
            </tbody>

            Trame test : <a href="visualiser_donnees.php?long=55.490112&lat=-21.341347">$GPGGA,115759.000,2120.4808,S,05529.4067,E,1,7,1.39,159.5,M,-9.3,M,,*57</a>
            <br /><br />
    </body>
</html>

