<?php

function hex2float($strHex) {
    $hex = sscanf($strHex, "%02x%02x%02x%02x%02x%02x%02x%02x");
    $hex = array_reverse($hex);
    $bin = implode('', array_map('chr', $hex));
    $array = unpack("dnum", $bin);
    return $array['num'];
}

// Converti une string en binaire
function str2bin($str) {
    $bin = '';
    $length = strlen($str);
    for ($i = 0; $i < $length; $i++) {
// On converti le code ASCII du char en binaire
        $convert = decbin(ord($str[$i]));

// On complète avec les 0 pour faire 1 octet
        $convert = strrev(str_pad(strrev($convert), 8, '0'));
        $bin .= $convert;
    }
    return ($bin);
}

function String2Hex($string){
    $hex='';
    for ($i=0; $i < strlen($string); $i++){
        $hex .= dechex(ord($string[$i]));
    }
    return $hex;
}
 
 function Hex2String($hex){
    $string='';
    for ($i=0; $i < strlen($hex)-1; $i+=2){
        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
    }
    return $string;
}
?>

