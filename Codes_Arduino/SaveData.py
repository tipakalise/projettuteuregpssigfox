#Programme permettant d'enregistrer les données reçues par le module dans un fichier .txt
import serial
ser = serial.Serial('COM8', 9600, timeout = 1)
f = open('test.txt','w+')
f.write("Acquisition des donnees")
 
print("Demarrage")
try:
    while 1:
        line = ser.readline()
        if line:
            print(line)
            f.write(line)
except KeyboardInterrupt:
    f.close()
    ser.close()
