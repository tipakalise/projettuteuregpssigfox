#include <MCP23008.h>
#include <multiprotocolShield.h>
#include <stdio.h>
#include <stdlib.h>
#include <structlibrary.h>
#include <math.h>;

#include <Wire.h>

// Cooking API libraries
#include <arduinoUART.h>
#include <arduinoUtils.h>

// Sigfox library
#include <arduinoSigfox.h>

//SoftwareSerial mySerial(0, 1); // RX, TX

// Pin definition for Sigfox module error LED:
const int error_led =  13;

//////////////////////////////////////////////
uint8_t socket = SOCKET0;     //Asign to UART0
//////////////////////////////////////////////

uint8_t error;

String myString = String ("$GPGGA,073059.000,2120.4283,N,05529.4779,W,1,5,1.56,122.9,M,-9.3,M,,*5B");
const char *result = myString.c_str();
struct GPSData myData;
uint8_t tableauDeByte[12];
char buff[256];
String message;

void setup() {
  //Configuration
  Serial.begin(9600);
  //mySerial.begin(9600);
  socket1.ON();
  delay(100);
  socket1.setCS();
  delay(100);
  socket1.setMUX();
  pinMode(error_led, OUTPUT);
  digitalWrite(error_led,LOW);

  Serial.println("Configuration terminee !");
  Serial.println("Lancement de la loop :");
  digitalWrite(error_led,HIGH);
  delay(5000);
  digitalWrite(error_led,LOW);
  
  //Parsement des données de la trame GPGGA et stockage dans des variable de type char*

  char * tps = strchr (result, ','); *tps = '\0'; ++tps;
  char * latd = strchr (tps, ','); *latd = '\0'; ++latd;
  char * card1 = strchr (latd, ','); *card1 = '\0'; ++card1;
  char * lngt = strchr (card1, ','); *lngt = '\0'; ++lngt;
  char * card2 = strchr (lngt, ','); *card2 = '\0'; ++card2;
  char * dataNonUtile = strchr (card2, ','); *dataNonUtile = '\0'; ++dataNonUtile;
  
  float temps=atof(tps);
  /*atof() permet la conversion en float, conv_coord() la conversion en dégrés décimal et comparerCard() le changement du signe
  en fonction de la valeur du cardinal correspondant*/
  
  float longitude = comparerCard(conv_coords(atof(lngt)),card1);
  float latitude = comparerCard(conv_coords(atof(latd)),card2);

  Serial.println(longitude);
  Serial.println(latitude);
  
  //float longitude = 55.536384f;
  //float latitude = -21.115141f;

  //Size float = 4 bytes
  sprintf("size of float in bytes : %d\n", sizeof(float));

  //struct GPSData myData;
  myData.temps = temps;
  myData.latitude = latitude;
  myData.longitude = longitude;

  sprintf("size of GPSData in bytes : %d\n", sizeof(myData));

  //uint8_t tableauDeByte[sizeof(myData)];

  memcpy(tableauDeByte, &myData, sizeof(myData));
  sprintf("size of tableauDeByte in bytes : %d\n", sizeof(tableauDeByte));

  //Conversion en hexadécimal
  for (uint8_t i = 0; i < sizeof(tableauDeByte); i++) {
    sprintf(buff,"0x%X", tableauDeByte[i]);
    message += buff;
  }
  Serial.println(message);
  // And back
  /*struct GPSData myDataBack;
  memcpy(&myDataBack, tableauDeByte, sizeof(tableauDeByte));

  Serial.println(myDataBack.latitude);
  
  /*sprintf("Longitude : GPSData : %f\n", myDataBack.longitude);
  sprintf("Latitude : GPSData : %f\n", myDataBack.latitude);
  */
  
  error = Sigfox.send(tableauDeByte, 12);
  // Check sending status
  if( error == 0 )
  {
    //"Sigfox packet sent OK"
    digitalWrite(error_led, LOW);
  }
  else
  {
    //"Sigfox packet sent ERROR"
    digitalWrite(error_led, HIGH);
  }
  
}


void loop(){
}

//cfeba8c1

//Librairie à créer
/*struct GPSData {
  float temps;  
  float latitude;
  float longitude;
};*/


float comparerCard(float coord, char card[]){
  char str2 [] = "S";
  char str3 [] = "W";
  float resultat;
  if (strcmp(card,str2)==0 || strcmp(card,str3)==0){
    resultat=0-coord;
    return resultat;
  }
  return coord;
}

//Conversion en degrés décimal

float conv_coords(float in_coords)
 {
 //Initialize the location.
 float f = in_coords;
 // Get the first two digits by turning f into an integer, then doing an integer divide by 100;
 // firsttowdigits should be 77 at this point.
 int firsttwodigits = ((int)f)/100; //This assumes that f < 10000.
 float nexttwodigits = f - (float)(firsttwodigits*100);
 float theFinalAnswer = (float)(firsttwodigits + nexttwodigits/60.0);
 return theFinalAnswer;
 }