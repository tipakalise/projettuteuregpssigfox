#include <multiprotocolShield.h> //Arduino shield library

#include <stdio.h> //Standard Input/Output library
#include <stdlib.h> //Standard library

#include <Wire.h> //Serial Communication library

// Cooking Hack API libraries
#include <arduinoUART.h>
#include <arduinoUtils.h>

// Sigfox library
#include <arduinoSigfox.h>

//Parsing Libraries
#include <structlibrary.h> //Created library
#include <math.h> //Arduino Math library


// Pin definition for Sigfox module error LED:
const int error_led =  13;
uint8_t socket = SOCKET0; //Asign to UART0
uint8_t error; //Send data to Sigfox

SoftwareSerial mySerial(0, 1); // RX, TX

////////////////////////////////////////////////////////////
//			Variables used to process the frame data	  //	
////////////////////////////////////////////////////////////

//GPGGA frame
String gpgga = String ("$GPGGA,073059.000,2120.4283,N,05529.4779,W,1,5,1.56,122.9,M,-9.3,M,,*5B");

const char *result = gpgga.c_str(); //Conversion to unsigned char type
struct GPSData myData; //Variable of type struct GPSData according to "structlibrary" library
uint8_t tableauDeByte[12]; //Byte Array (12-byte size)
char buff[256]; //Char array (buffer size of 256 characters)
String message; //Use to display the hexadecimal conversion


void setup() {
  //Configuration
  
  //Sets the data rate in bits per second (baud) for serial data transmission
  Serial.begin(9600);
  mySerial.begin(9600);
  
  //Configuration of the Multiprotocol Radio Shield
  socket1.ON(); ////Switches ON the power supply of the socket1
  delay(100); //wait 100ms
  socket1.setCS(); //Enables the SPI of the socket1 (5V Level)
  delay(100);
  socket1.setMUX(); //Configure the multiplexor in the socket1
  
  pinMode(error_led, OUTPUT); //Sets the digital pin 13 (error_led) as output
  digitalWrite(error_led, LOW); //Sets the digital pin 13 (error_led) to LOW

  Serial.println("Configuration terminee !");
  Serial.println("Lancement de la loop :");
  digitalWrite(error_led, HIGH); //Sets the digital pin 13 (error_led) to HIGH
  delay(5000); //wait 5000ms
  digitalWrite(error_led, LOW); //Sets the digital pin 13 (error_led) to LOW

  //GPGGA frame parsing data thanks to the delimiter "," and storage in unsigned char variables

  //Extract the time data
  char * tps = strchr (result, ','); *tps = '\0'; ++tps;
  //Extract the latitude data
  char * latd = strchr (tps, ','); *latd = '\0'; ++latd;
  //Extract the Cardinal points of the latitude data (South or North) 
  char * card1 = strchr (latd, ','); *card1 = '\0'; ++card1;
  //Extract the longitude data
  char * lngt = strchr (card1, ','); *lngt = '\0'; ++lngt;
  //Extract the Cardinal points of the longitude data (East or West) 
  char * card2 = strchr (lngt, ','); *card2 = '\0'; ++card2;
  char * dataNonUtile = strchr (card2, ','); *dataNonUtile = '\0'; ++dataNonUtile;
  char * dataNonUtile =0; //Data not used

  Serial.print("GPS Frame : ");
  Serial.println(gpgga); //Display GPGGA frame
  
  //Size of float is 32bits (4 bytes)
  float temps = atof(tps); //The atof function allows the conversion to float
  
  //* The conv_coord function allows the conversion to decimal degrees
  //* The comparerCard function returns the input value in negative value 
  //according to the cardinal if it is south or west
  
  //Nota : The conv_coord and comparerCard functions are to be created
  float longitude = comparerCard(conv_coords(atof(lngt)), card1);
  float latitude = comparerCard(conv_coords(atof(latd)), card2);

  //Display extracted pieces of information
  Serial.print("Time : ");
  Serial.println(temps);
  Serial.print("Longitude : ");
  Serial.println(longitude);
  Serial.print("Latitude : ");
  Serial.println(latitude);

  //Asign values to the members of the structure
  myData.temps = temps;
  myData.latitude = latitude;
  myData.longitude = longitude;
  
  //The memcpy function allows to copy a block of memory from a location to another
  //Here, we copy the data from myData to tableauDeByte
  memcpy(tableauDeByte, &myData, sizeof(myData));

  //Convert to hexadecimal
  //Convert the data from tableauDeByte to hexadecimal and store it 
  //in a char array variable
  for (uint8_t i = 0; i < sizeof(tableauDeByte); i++) {
    sprintf(buff, "%X", tableauDeByte[i]);
    message += buff; //Storage converted data to display it thanks to a String variable
  }
  
  Serial.print("\n");
  Serial.print("Conversion to hexadecimal : ");
  Serial.println(message); //Display the converted data
}


void loop() {
  error = Sigfox.ON(socket);

  // Check status
  if ( error == 0 ){
    //"Switch ON OK"
    digitalWrite(error_led, LOW);
  }
  else{
    //"Switch ON ERROR"
    digitalWrite(error_led, HIGH);
  }

  Serial.println("Sending to Sigfox...");
  //Sending data to Sigfox
  error = Sigfox.send(tableauDeByte);
  Serial.println("");

  //Check sending status
  if ( error == 0 ){
    //"Sigfox packet sent OK"
    digitalWrite(error_led, LOW);
  }
  else{
    //"Sigfox packet sent ERROR"
    digitalWrite(error_led, HIGH);
  }
}

//Résultat à retrouver lors du décodage :
//Longitude : 55.49
//Latitude : -21.34

//Résultat avant décodage : 80B18E474BB9AAC117F75D42

//"stuctgpsdata.h" library
/*struct GPSData {
  float temps;
  float latitude;
  float longitude;
};*/

//////////////////////////////////////////////////////////////
//						Functions used					    //
//////////////////////////////////////////////////////////////

//The comparerCard function returns the input value in negative value 
//according to the cardinal if it is south or west
float comparerCard(float coord, char card[]){
  char str2 [] = "S";
  char str3 [] = "W";
  float resultat;
  if (strcmp(card, str2) == 0 || strcmp(card, str3) == 0) {
    resultat = 0 - coord;
    return resultat;
  }
  return coord;
}

//The conv_coord function allows the conversion to decimal degrees
float conv_coords(float in_coords){
  //Initialize the location
  float f = in_coords;
  //Get the first two digits by turning f into an integer, then doing an integer divide by 100;
  //firsttowdigits should be 77 at this point.
  int firsttwodigits = ((int)f) / 100; //This assumes that f < 10000.
  float nexttwodigits = f - (float)(firsttwodigits * 100);
  float theFinalAnswer = (float)(firsttwodigits + nexttwodigits / 60.0);
  return theFinalAnswer;
}