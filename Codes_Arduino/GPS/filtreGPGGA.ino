#include <SoftwareSerial.h>
#include <MCP23008.h>
#include <multiprotocolShield.h>

SoftwareSerial mySerial(0, 1); // RX, TX
String message;

void setup() {
  //Configuration
  Serial.begin(9600);
  mySerial.begin(9600);
  socket1.ON();
  delay(100);
  socket1.setCS();
  delay(100);
  socket1.setMUX();

  Serial.println("Configuration terminee !");
  Serial.println("Lancement de la loop :");
  //mySerial.println("$PMTK353,1,1*37");
  //delay (100);
  //mySerial.println( F("$PMTK 314 ,0,0,0,0,0,3,0,0,0,0,0,0,0,0,1,0,0,0,0 *34"));
  delay (100);
}

void loop()
{
  //Serial.print("Trame gps :");
  char carac = 0;
  // boucle qui attends un \n pour valider la trame et la décoder (/!\ Passer l'option en bas à droite du moniteur série en "Nouvelle ligne")
  while (Serial.available())

  {
    // lecture d'un caractère
    carac = Serial.read();
    // concaténation du caractère au message
    //message = message + carac;


    if (carac == '$') {
      message = message+"\n";
      message= "";
      //Serial.println(message.indexOf("$"));
      
    }else{
      message = message + carac;
    }

    Serial.println(message);
      
  }
}