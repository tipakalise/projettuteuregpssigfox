#include <SoftwareSerial.h>
#include <MCP23008.h>
#include <multiprotocolShield.h>

SoftwareSerial mySerial(0, 1); // RX, TX
String message;

void setup() {
  //Configuration
  Serial.begin(9600);
  mySerial.begin(9600);
  socket1.ON();
  delay(100);
  socket1.setCS();
  delay(100);
  socket1.setMUX();
 
  Serial.println("Configuration terminee !");
  Serial.println("Lancement de la loop :");
  mySerial.println("$PMTK353,1,1*37");
}

void loop()
{
  //Serial.print("Trame gps :");
  Serial.print(message);
  char carac = 0;
  // boucle qui attends un \n pour valider la trame et la décoder (/!\ Passer l'option en bas à droite du moniteur série en "Nouvelle ligne")
  while (carac != '\n')

  {
    // si un caractère est présent sur la liaison
    if (Serial.available())
    {
      // lecture d'un caractère
      carac = Serial.read();
      // concaténation du caractère au message
      message = message + carac;
    }
  }

  Serial.print (message);

  if (message.substring(0, 6) == "$GPGGA")
  {
    //Serial.print("Trame GPGGA Recue : ");
    //Serial.println(message);

    // Décodage de la trame
    //temps
    String temps;   temps = temps + message.substring(7, 13);
    Serial.println ("Temps: " + temps);
    
    // latitude
    String latitude;   latitude = latitude + message.substring(18, 27);   if (message[30] == 'N')  latitude = latitude + " Nord";   else      latitude = latitude + " Sud";    Serial.println ("Latitude: " + latitude);

    // longitude
    String longitude;   longitude = longitude + message.substring(31, 40); if (message[43] == 'W')  longitude = longitude + " Ouest";   else longitude = longitude + " Est"; Serial.println ("Longitude: " + longitude);
  }
  if (message.substring(0, 6) != "$GPGGA") {
    Serial.println("");
  }

  else
    Serial.println("");


  message = "";

}
