#include <SoftwareSerial.h>
#include <MCP23008.h>
#include <multiprotocolShield.h>
#include <stdio.h>
#include <string.h>

SoftwareSerial mySerial(0, 1); // RX, TX
char gps[]="$GPGGA,073059.000,2120.4283,S,05529.4779,E,1,5,1.56,122.9,M,-9.3,M,,*5B";
char *B;
B = strtok(gps,",");
int count = 0;
char *data[24];

void setup() {
  //Configuration
  Serial.begin(9600);
  mySerial.begin(9600);
  socket1.ON();
  delay(100);
  socket1.setCS();
  delay(100);
  socket1.setMUX();

  Serial.println("Configuration terminee !");
  Serial.println("Lancement de la loop :");


  while(B!=NULL){         
    B = strtok(NULL,",");
    if(B!=NULL){
      data[count++]=B;
    }
  }
  Serial.println("Heure :");
  Serial.println(data[0]);
  Serial.println("Latitude :");
  Serial.println(data[1]);
  Serial.println("Longitude :");
  Serial.println(data[3]);

  strcat((char *)data[0],(char *)data[1]);
  strcat((char *)data[0],(char *)data[2]);
  strcat((char *)data[0],(char *)data[3]);
  strcat((char *)data[0],(char *)data[4]);
  Serial.println("\nDonnées utiles :");
  Serial.println(data[0]);


}

void loop() {}