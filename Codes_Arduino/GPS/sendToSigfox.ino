#include <Wire.h>

// Cooking API libraries
#include <arduinoUART.h>
#include <arduinoUtils.h>

// Sigfox library
#include <arduinoSigfox.h>

// Pin definition for Sigfox module error LED:
const int error_led =  13;

//////////////////////////////////////////////
uint8_t socket = SOCKET0;     //Asign to UART0
//////////////////////////////////////////////

uint8_t error;


void setup()
{
  pinMode(error_led, OUTPUT);
}


void loop() 
{  

  //////////////////////////////////////////////
  // 1. switch on
  //////////////////////////////////////////////
  error = Sigfox.ON(socket);

  // Check status
  if( error == 0 )
  {
    //"Switch ON OK"
    digitalWrite(error_led, LOW);
  }
  else
  {
    //"Switch ON ERROR"
    digitalWrite(error_led, HIGH);
  }

  //////////////////////////////////////////////
  // 2. send data
  //////////////////////////////////////////////

  char data [] = "07305921204283S055294779E";
  int tailleData = sizeof(data);
  char stock[tailleData];
  String message;
  char*st;
  Serial.println("");
  Serial.println(data);
  for(int i=0;i<tailleData;i++){
    sprintf(stock,"%2X",data[i]);
    message += stock;
    //Serial.println(data[i]);
  }
  Serial.println(message);
  message.toCharArray(stock,tailleData+1);
  // Send 12 bytes at most
  error = Sigfox.send((char *) stock);

  // Check sending status
  if( error == 0 )
  {
    //"Sigfox packet sent OK"
    digitalWrite(error_led, LOW);
  }
  else
  {
    //"Sigfox packet sent ERROR"
    digitalWrite(error_led, HIGH);
  }

  //////////////////////////////////////////////
  // 3. sleep
  //////////////////////////////////////////////
  delay(60000);
}