#include <SoftwareSerial.h>
#include <MCP23008.h>
#include <multiprotocolShield.h>

SoftwareSerial mySerial(0, 1); // RX, TX
String trame="";
int i;
int compteur_virgule;
int compteur_dollar;

void setup() {
  //Configuration
  Serial.begin(9600);
  mySerial.begin(9600);
  socket1.ON();
  delay(100);
  socket1.setCS();
  delay(100);
  socket1.setMUX();

  //Clignotement de la LED
  digitalWrite(LED_BUILTIN, HIGH);   //Allumer LED
  delay(100);
  digitalWrite(LED_BUILTIN, LOW);    //Eteindre LED
  delay(100);

  Serial.println("Configuration terminee !");
  Serial.println("Lancement de la loop :");
  mySerial.println("$PMTK353,1,1*37");

}

void loop() { // run over and over
  compteur =0;
  trame = mySerial.read();

 for (i=0; i<trame.length();i++){
  if (trame[i]=='$'){
    Serial.println("Debut de trame");
    compteur_dollar++;
  }
    if (trame[i]==','){
      compteur_virgule++;
    }
 }
 Serial.println(trame);
 Serial.println("nombre de virgules présentes : " + compteur);
}
