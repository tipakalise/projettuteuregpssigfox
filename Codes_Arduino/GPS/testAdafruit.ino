#include <SoftwareSerial.h>
#include <MCP23008.h>
#include <multiprotocolShield.h>
#include <NMEA.h>
#include <Adafruit_GPS.h>
#include <SPI.h>

SoftwareSerial mySerial(0, 1); // RX, TX
Adafruit_GPS GPS(&mySerial);
String NMEA1; //Variable for first NMEA sentence
String NMEA2; //Variable for second NMEA sentence
char c; //to read characters coming from the GPS


void setup() {
  //Configuration
  Serial.begin(9600);
  mySerial.begin(9600);
  GPS.begin(9600);
  socket1.ON();
  delay(100);
  socket1.setCS();
  delay(100);
  socket1.setMUX();
  Serial.println("Configuration terminee !");
  Serial.println("Lancement de la loop :");
  GPS.sendCommand("$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29");
  //GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);
  delay (100);
}

void loop() {
    readGPS();
}

void readGPS() {
  
  clearGPS();
  while(!GPS.newNMEAreceived()) { //Loop until you have a good NMEA sentence
    c=GPS.read();
  }
  GPS.parse(GPS.lastNMEA()); //Parse that last good NMEA sentence
  NMEA1=GPS.lastNMEA();
  
   while(!GPS.newNMEAreceived()) { //Loop until you have a good NMEA sentence
    c=GPS.read();
  }
  GPS.parse(GPS.lastNMEA()); //Parse that last good NMEA sentence
  NMEA2=GPS.lastNMEA();
  
  Serial.println(NMEA1);
  Serial.println(NMEA2);
  Serial.println("");
  
}
 
void clearGPS() {  //Clear old and corrupt data from serial port 
  while(!GPS.newNMEAreceived()) { //Loop until you have a good NMEA sentence
    c=GPS.read();
  }
  GPS.parse(GPS.lastNMEA()); //Parse that last good NMEA sentence
  
  while(!GPS.newNMEAreceived()) { //Loop until you have a good NMEA sentence
    c=GPS.read();
  }
  GPS.parse(GPS.lastNMEA()); //Parse that last good NMEA sentence
   while(!GPS.newNMEAreceived()) { //Loop until you have a good NMEA sentence
    c=GPS.read();
  }
  GPS.parse(GPS.lastNMEA()); //Parse that last good NMEA sentence
  
}
