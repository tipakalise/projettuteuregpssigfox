#include <SoftwareSerial.h>
#include <MCP23008.h>
#include <multiprotocolShield.h>

SoftwareSerial mySerial(0, 1); // RX, TX

void setup() {
  //Configuration
  Serial.begin(9600);
  mySerial.begin(9600);
  socket1.ON();
  delay(100);
  socket1.setCS();
  delay(100);
  socket1.setMUX();

  //Clignotement de la LED
  digitalWrite(LED_BUILTIN, HIGH);   //Allumer LED
  delay(100);                       
  digitalWrite(LED_BUILTIN, LOW);    //Eteindre LED
  delay(100);                       
 
  Serial.println("Configuration terminee !");
  Serial.println("Lancement de la loop :");
  mySerial.println("$PMTK353,1,1*37");
  
}

void loop() { // run over and over
  if (mySerial.available()) {
    
    Serial.write(mySerial.read());
  }
  {
 // message= analogRead(0);
  //Serial.println("Trame gps :");
  Serial.print(message);
  char carac = 0;
  // boucle qui attends un \n pour valider la trame et la décoder (/!\ Passer l'option en bas à droite du moniteur série en "Nouvelle ligne")
  while (carac != '\n')

  {
    // si un caractère est présent sur la liaison
    if(Serial.available())
    {
      // lecture d'un caractère
      carac = Serial.read();
      // concaténation du caractère au message
      message = message + carac; 
    }
  }
  
  Serial.print (message);
  
  //Serial.println("Message Recu !"); 
  
  if (message.substring(0,6) == "$GPGGA")
  {
    Serial.println("Trame GPRMC Recue");
    Serial.println(message);
   
  }
   if (message.substring(0,6) != "$GPGGA"){ Serial.println(""); 
  delay(60000);
}
