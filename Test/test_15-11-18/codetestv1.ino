#include <SoftwareSerial.h>
#include <MCP23008.h>
#include <multiprotocolShield.h>
#include <Adafruit_gps.h>

SoftwareSerial mySerial(0, 1); // RX, TX
Adafruit_gps gps(&mySerial);

void setup() {
  //Configuration
  Serial.begin(9600);
  mySerial.begin(9600);
  socket1.ON();
  delay(100);
  socket1.setCS();
  delay(100);
  socket1.setMUX();

  Serial.println("Configuration terminee !");
  Serial.println("Lancement de la loop :");
  mySerial.println("$PMTK353,1,1*37");
  gps.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  gps.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);
  gps.sendCommand(PGCMD_ANTENNA);
}

void loop() {
  if (mySerial.available()) {
    gps.parse(gps.lastNMEA());
    gps.newNMEAreceived();
    Serial.print("\nTime: ");
    Serial.print(gps.hour, DEC); Serial.print(':');
    Serial.print(gps.minute, DEC); Serial.print(':');
    Serial.print(gps.seconds, DEC); Serial.print('.');
    Serial.println(gps.milliseconds);
    Serial.print("Date: ");
    Serial.print(gps.day, DEC); Serial.print('/');
    Serial.print(gps.month, DEC); Serial.print("/20");
    Serial.println(gps.year, DEC);
    Serial.print("Fix: "); Serial.print((int)gps.fix);
    Serial.print(" quality: "); Serial.println((int)gps.fixquality);
    if (gps.fix) {
      Serial.print("Location: ");
      Serial.print(gps.latitude, 4); Serial.print(gps.lat);
      Serial.print(", ");
      Serial.print(gps.longitude, 4); Serial.println(gps.lon);
      Serial.print("Location (in degrees, works with Google Maps): ");
      Serial.print(gps.latitudeDegrees, 4);
      Serial.print(", ");
      Serial.println(gps.longitudeDegrees, 4);
      Serial.print("Speed (knots): "); Serial.println(gps.speed);
      Serial.print("Angle: "); Serial.println(gps.angle);
      Serial.print("Altitude: "); Serial.println(gps.altitude);
      Serial.print("Satellites: "); Serial.println((int)gps.satellites);
    }
  }

}
