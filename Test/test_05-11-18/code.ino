#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <MCP23008.h>
#include <multiprotocolShield.h>

SoftwareSerial mySerial(0, 1); // RX, TX
TinyGPSPlus gps;

void setup() {
  //Configuration
  Serial.begin(9600);
  mySerial.begin(9600);
  socket1.ON();
  delay(100);
  socket1.setCS();
  delay(100);
  socket1.setMUX();

  Serial.println("Configuration terminee !");
  Serial.println("Lancement de la loop :");
  mySerial.println("$PMTK353,1,1*37");
}

void loop() {
  if (mySerial.available()) {
    //Serial.write(mySerial.read());
    if(gps.encode(mySerial.read())){
      TinyGPSCustom gpgga(gps, "GPGGA", 3);
      if(gpgga.isUpdated()){
        Serial.write(gpgga.value());
      }
    }
  }

}
